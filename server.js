
const puppeteer = require('puppeteer');
const fileAttente = require('./fileAttente.js');
const followManager = require('./utility/followManager.js');
const connectionManager = require('./utility/connectionManager.js');
const tweetManager = require('./utility/tweetManager.js');




require('dotenv').config();

const timeout = 3000;
// const maxTimeout = 20000;



console.log("Session lauched for: " + process.env.NAME);
// console.log(process.env.PASSWORD);



(async function main() {

  try {
    const browser = await puppeteer.launch({
      headless: false,
      args: [
        '--start-maximized'
      ],
    });


    // const context = await browser.createIncognitoBrowserContext();


    const page = await browser.newPage();
    await page.setViewport({ width: 1366, height: 768 });


    page.setUserAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36");
    // await page.setDefaultNavigationTimeout(maxTimeout);
    await connectionManager.setCookie(page);





    await page.goto("https://twitter.com");

    await page.waitFor(timeout);
    //If we are note connected
    const isCo = await connectionManager.isConnect(page);
    if (!isCo) {
      fileAttente.addTask(connectionManager.connect, [page, process.env.NAME, process.env.PASSWORD]);
    } else {
      console.log("Already connected");
    }
    await page.waitFor(timeout);
    //  fileAttente.addTask(tweetManager.retweetWithTerm,[page,"Rihanna"]);
    
    //  fileAttente.addTask(followManager.followAllFollowers,[page, "keita_dabi"]);
    // fileAttente.addTask(followManager.follow,[page, "blackpuna"]);
    //fileAttente.addTask(followManager.followAllFollowers,[page,"Kotazoo_"]); 



  } catch (e) {
    console.log("We have an error " + e);
  }

})()


