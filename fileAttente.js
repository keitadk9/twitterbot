const fileAttente = [];
let isRunning = false;
const taskBreak = 2000;
//this function is recursive and while we have task to do he call itself
async function executeTask() {

    if (fileAttente.length > 0) {
        const tache = fileAttente[0];
        isRunning = true;
        await tache.function(tache.parameters);
        fileAttente.shift();
        console.log(`${fileAttente.length} task leafted`);
        setTimeout(() => {
            executeTask();
        }, taskBreak);

    } else {
        isRunning = false;
        console.log("Done")
    }

}


function addTask(functionCall, parameters) {
    fileAttente.push({
        'function': functionCall,
        'parameters': parameters
    });
    console.log("One task added");

    if (!isRunning) {
        console.log("Running");
        executeTask();
    }
}


module.exports = {
    executeTask,
    addTask
}