const fileAttente = require('../fileAttente.js');
const searchElement = require('./searchElement.js')
const scrollPageToBottom = require('puppeteer-autoscroll-down');
require('dotenv').config();


const timeout = 5000; // timeout for the page loading
const followTimeout = 3600000 * 48; //48h


async function follow(parameters) {

    const page = parameters[0];
    const name = parameters[1];
    console.log("Try to follow " + name);


    await page.goto(`https://twitter.com/${name}`);
    await page.waitFor(timeout);
    let div = await searchElement.findDivByText(page, "Following", true);
    // console.log(div);
    //We check if we already follow the guys
    if (div) {
        console.log("Already followed")
        return;
    }

    div = await searchElement.findDivByText(page, "Follow", true, true);
    if (div) {
        // console.log(div);
        div.click();
        console.log("follow clear " + name);

        //This will be executed after the timeout
        setTimeout(() => {
            fileAttente.addTask(checkFollow, [page, name]);
        }, followTimeout);
    }
}


async function unfollow(parameters) {

    const page = parameters[0];
    const name = parameters[1];

    await page.goto(`https://twitter.com/${name}`);
    console.log("try to unfollow " + name);

    await page.waitFor(timeout);

    div = await searchElement.findDivByText(page, "Following", true, true);
    if (div) {
        await div.click();
        divs = await searchElement.findDivByTextContain(page, "Unfollow", false, true);
        const unfollowButton = divs[divs.length - 1];

        await unfollowButton.click();
        console.log("Unfollowed clear " + name);
    } else {
        console.log("Not followed " + name);
    }
}

async function followAllFollowers(parameters) {

    const page = parameters[0];
    const name = parameters[1];

    await page.goto(`https://twitter.com/${name}/followers`);
    await page.waitFor(timeout);

    //  await scrollPageToBottom(page);

    let friendList = await searchElement.findDivByProperties(page, "dir=ltr", false);

    for (let i = 0; i < friendList.length; i++) {
        const friend = friendList[i];
        friendList[i] = await page.evaluate(friend => friend.innerText, friend);
    }


    let position = 0;
    let totalHeigth = await page.evaluate(() => document.body.scrollHeight);


    while (position < totalHeigth) {
        await page.evaluate(() => {
            window.scrollBy(0, 500);
        });
        position += 500;
        await page.waitFor(500);
        totalHeigth = await page.evaluate(() => document.body.scrollHeight);

        const liste = await searchElement.findDivByProperties(page, "dir=ltr", false);

        for (friend of liste) {
            friendName = await page.evaluate(friend => friend.innerText, friend);

            if (!friendList.includes(friendName)) {
                friendList.push(friendName);
                // console.log("new");
            }
        }
    }






    console.log(friendList.length);
    //We take out the third last because they only are twitter suggestion there are 3 of them
    if (friendList.length > 3) {
        friendList = friendList.slice(0, friendList.length - 3);
    }


    for (let i = 0; i < friendList.length; i++) {

        friend = friendList[i];
        //We dont want to follow ourself
        if (friend.toLocaleLowerCase() == `@${process.env.NAME}`.toLocaleLowerCase()) {
            continue;
        }

        //We take out the @
        friend = friend.replace("@", "");
        // console.log(friend);
        fileAttente.addTask(follow, [page, friend]);
    }
}

async function followAllFollow(parameters) {

    const page = parameters[0];
    const name = parameters[1];

    await page.goto(`https://twitter.com/${name}/following`);
    await page.waitFor(timeout);



    let friendList = await searchElement.findDivByProperties(page, "dir=ltr", false);

    for (let i = 0; i < friendList.length; i++) {
        const friend = friendList[i];
        friendList[i] = await page.evaluate(friend => friend.innerText, friend);
    }


    let position = 0;
    let totalHeigth = await page.evaluate(() => document.body.scrollHeight);


    while (position < totalHeigth) {
        await page.evaluate(() => {
            window.scrollBy(0, 500);
        });
        position += 500;
        await page.waitFor(500);
        totalHeigth = await page.evaluate(() => document.body.scrollHeight);

        const liste = await searchElement.findDivByProperties(page, "dir=ltr", false);

        for (friend of liste) {
            friendName = await page.evaluate(friend => friend.innerText, friend);

            if (!friendList.includes(friendName)) {
                friendList.push(friendName);
                // console.log("new");
            }
        }
    }

    console.log(friendList.length);
    //We take out the third last because they only are twitter suggestion there are 3 of them
    if (friendList.length > 3) {
        friendList = friendList.slice(0, friendList.length - 3);
    }


    for (let i = 0; i < friendList.length; i++) {

        friend = friendList[i];
        //We dont want to follow ourself
        if (friend.toLocaleLowerCase() == `@${process.env.NAME}`.toLocaleLowerCase()) {
            continue;
        }

        //We take out the @
        friend = friend.replace("@", "");
        // console.log(friend);
        fileAttente.addTask(follow, [page, friend]);
    }
}



async function checkFollow(parameters) {
    const page = parameters[0];
    const name = parameters[1]

    await page.goto(`https://twitter.com/${process.env.NAME}/followers`);
    await page.waitFor(timeout);
    //We search a div with the name on it
    const div = await searchElement.findDivByTextContain(page, name, true, false);

    //if we don't find the rigth div we unfollow the guys
    if (!div) {
        console.log("Not found");
        fileAttente.addTask(unfollow, [page, name]);
    }
}



module.exports = {
    follow,
    unfollow,
    followAllFollowers,
    followAllFollow
}