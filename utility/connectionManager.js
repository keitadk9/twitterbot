
const fs = require('fs');
const jsonfile = require('jsonfile');
const searchElement = require('./searchElement.js')
let connectStatus = false;
const timeout = 5000; // timeout for the page loading



async function connect(parameters) {



    const page = parameters[0];
    const username = parameters[1];
    const password = parameters[2];


    const fields = await page.$$(".LoginForm .LoginForm-input input");
    const usernameField = fields[0];
    const passwordField = fields[1];

    await usernameField.click();
    await page.keyboard.type(username);


    await passwordField.click();
    await page.keyboard.type(password);

    const button = await page.$(".EdgeButton");

    button.click()

    await page.waitFor(timeout);

    const isCo = await isConnect(page);
    console.log(isCo);


    if (isCo) {
        connectStatus = true;
    } else {
        console.log("Erreur de connection");
        connectSecondLevel(parameters);
    }

    await page.waitFor(timeout);

    const cookies = await page.cookies();
    jsonfile.writeFile('./utility/cookies.json', cookies, { spaces: 2 }, (err) => {
        if (err) {
            console.log(err);
        } else {
            console.log("Cookies saved");
        }
    })


}



async function connectSecondLevel(parameters) {

    const page = parameters[0];
    const password = parameters[2];
    const mail = process.env.MAIL;


    const usernameField = await page.$(".js-username-field");
    const passwordField = await page.$('.js-password-field');

    await usernameField.click();
    await page.keyboard.type(mail);


    await passwordField.click();
    await page.keyboard.type(password);



    const button = await page.$(".submit.EdgeButton.EdgeButton--primary.EdgeButtom--medium");
    await button.click();
}
//check if the username and password allowed the connection
async function isConnect(page) {
    const result = await page.$(".EdgeButton");
    // console.log(result);
    return result == null;
}



async function setCookie(page) {
    const result = fs.existsSync('./utility/cookies.json')
    console.log(result);
    if (result) {
        const cookiesArr = require('./cookies.json')
        if (cookiesArr.length !== 0) {
            for (let cookie of cookiesArr) {
                await page.setCookie(cookie)
            }
            console.log('Session has been loaded in the browser');
        }
    }
}


module.exports = {
    connect,
    connectSecondLevel,
    isConnect,
    setCookie
}