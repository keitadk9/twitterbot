const fileAttente = require('../fileAttente.js');
const searchElement = require('./searchElement.js');
const db = require('../db/db.js');

const retweetHastag = db.get('retweetHastag');




require('dotenv').config();

const timeout = 7000;
const nbScroll = 10;
const breakBetweenTweet = 2000;
const maxTweetOnce = 5;


//Retweet the 5 latest tweet with the hastag
async function retweetWithTerm(parameters) {
    const page = parameters[0];
    const hastag = parameters[1];


    hastag.replace('#','%23');

    await page.goto(`https://twitter.com/search?q=${hastag}&src=typed_query`);
    // console.log("ok");
    await page.waitFor(timeout);


    for (let i = 0; i < nbScroll; i++) {
        await page.waitFor(200);
        await page.evaluate(() => {
            window.scrollBy(0, 250);
        })
    }

    const listeTweet = await searchElement.getAllTweetOnPage(page);
    // console.log(listeTweet);


    if (listeTweet.length > 0) {

        const tuple = await retweetHastag.findOne({ 'hastag': hastag });


        // console.log("1")

        if (tuple) {
            for (let i = 0; i < Math.min(listeTweet.length, maxTweetOnce); i++) {
                try {
                    console.log("Tweet " + i);
                    const tweet = listeTweet[i];

                    if ((new Date(tweet.date)) - (new Date(tuple.date)) < 0) {
                        await tweet.button.click();
                        const buttonConfirm = await searchElement.findDivByProperties(page, "data-testid=retweetConfirm", true);
                        await buttonConfirm.click();
                        await page.waitFor(breakBetweenTweet);
                        console.log("Clear");
                    }else{
                        console.log('break')
                        break;
                    }
                } catch (e) {
                    console.log("Erreur " + e)
                }
            }


        } else {
            // console.log("2")
            const latestTweet = new Date(listeTweet[0].date);
            await retweetHastag.insert({
                'hastag': hastag,
                'date': latestTweet.toString()
            });

            for (let i = 0; i < Math.min(listeTweet.length, maxTweetOnce); i++) {
                try {
                    console.log("Tweet " + i);
                    const tweet = listeTweet[i];

                    await tweet.button.click();

                    const buttonConfirm = await searchElement.findDivByProperties(page, "data-testid=retweetConfirm", true);
                    // console.log("btn confirm");
                    // console.log(buttonConfirm);
                    await buttonConfirm.click();
                    await page.waitFor(breakBetweenTweet);
                    console.log("Clear");

                } catch (e) {
                    console.log("Erreur " + e)
                }
            }
        }
    }
}


async function retweetLatestTweet(parameters) {

    const page = parameters[0];
    const name = parameters[1];

    await page.goto(`https://twitter.com/${name}`);
    page.waitFor(timeout)

    for (let i = 0; i < nbScroll; i++) {
        await page.waitFor(200);
        await page.evaluate(() => {
            window.scrollBy(0, 250);
        })
    }

    //After we scrolled we zoom out to see all the tweet we retrieve the tweet

    // await page.evaluate(() => {
    //     document.body.style.zoom = "25%"
    // });

    await page.waitFor(timeout);
    listeTweet = await searchElement.getAllTweetOf(page, name);

    console.log(listeTweet);

    console.log("Retweet of " + name);

    for (let i = 0; i < Math.min(listeTweet.length, maxTweetOnce); i++) {
        try {
            console.log("Tweet " + i);
            const tweet = listeTweet[i];

            await tweet.button.click();

            const buttonConfirm = await searchElement.findDivByProperties(page, "data-testid=retweetConfirm", true);
            // console.log("btn confirm");
            // console.log(buttonConfirm);
            await buttonConfirm.click();
            await page.waitFor(breakBetweenTweet);
            console.log("Clear");

        } catch (e) {
            console.log("Erreur " + e)
        }
    }

    // await page.evaluate(() => {
    //     document.body.style.zoom = "100%"
    // });
}



module.exports = {
    retweetLatestTweet,
    retweetWithTerm
}