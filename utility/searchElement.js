const db = require('../db/db.js');

//List of all people that we retweet all tweet
const followMirroir = db.get('followMirroir');



async function findDivByText(page, searchVal, oneElement, asButton) {
    const divs = await page.$$(`${asButton == true ? 'div[role=button]' : 'div'}`);
    const liste = [];


    // console.log(divs.length);

    for (let i = 0; i < divs.length; i++) {

        const div = divs[i];

        const text = await page.evaluate(div => div.textContent, div);
        if (text.trim() == searchVal) {
            //  console.log(text);
            if (oneElement) {
                return div;
            } else {
                liste.push(div);
            }
        }
        //  console.log("//////////////////////");
    } if (!oneElement) {
        return liste;
    }
    return null;
}

//Search and return the concerned div if the searchVal is part of the text (it work like a filter)
async function findDivByTextContain(page, searchVal, oneElement) {
    const divs = await page.$$(`div`);
    const liste = [];


    // console.log(divs.length);

    for (let i = 0; i < divs.length; i++) {

        const div = divs[i];

        const text = await page.evaluate(div => div.innerText, div);

        if (text.split(" ").includes(searchVal) || text.split("↵").includes(searchVal)) {
            // console.log(text);
            if (oneElement) {
                return div;
            } else {
                liste.push(div);
            }
        }
        // console.log("//////////////////////");
    }
    if (!oneElement) {
        return liste;
    }
    return null;
}



async function findDivByProperties(page, propertie, oneElement) {
    const divs = await page.$$(`div[${propertie}]`);


    if (oneElement) {
        if (divs.length > 0) {
            return divs[0];
        } else {
            return null;
        }
    } else {
        return divs
    }

}



async function getAllTweetOnPage(page) {


    const retweetButton = await page.$$('article div[data-testid=retweet]');
    const tweetDate = await page.$$('article a time');

    for (let i = 0; i < tweetDate.length; i++) {
        date = tweetDate[i]
        const dateClean = await page.evaluate(date => date.dateTime, date);
        tweetDate[i] = dateClean
    };

    const liste = [];

    for (let i = 0; i < tweetDate.length; i++) {
        const date = new Date(tweetDate[i]);
        const button = retweetButton[i];
        liste.push({
            button,
            'date': date.toString()
        });

    }

    return liste;


}


const maxDayRetweet = 2;
async function getAllTweetOf(page, name) {

    const retweetButton = await page.$$('article div[data-testid=retweet]');
    const tweetDate = await page.$$('article a time');

    for (let i = 0; i < tweetDate.length; i++) {
        date = tweetDate[i]
        const dateClean = await page.evaluate(date => date.dateTime, date);
        tweetDate[i] = dateClean

    }

    listeTweet = [];


    user = await followMirroir.findOne({ 'username': name })


    if (user) {
        // console.log(retweetButton.length);
        // console.log(tweetDate.length);
        // console.log(tweetDate);

        const latestDate = new Date(user.latestDate);
        let newLatestDate = new Date(user.latestDate);



        //The tweetDate and retweet button as the same size and are mirroir of each other
        for (let i = 0; i < retweetButton.length; i++) {
            // console.log("ok");
            const date = new Date(tweetDate[i]);
            const button = retweetButton[i];


            //If the soustraction is lower than 0 that mean that the tweet is more recent than the latest one
            //And we make sure that he is not older than 3 maxDayRetweet
            if (date - latestDate < 0 && (new Date()) - date < (3600000 * 24) * maxDayRetweet) {
                listeTweet.push({
                    button,
                    'date': date.toString()
                });

                if (date - newLatestDate < 0) {
                    newLatestDate = date;
                }

            } else {
                break;
            }
        }
        await followMirroir.update({ 'username': name }, { $set: { 'latestDate': newLatestDate.toString() } });
        // console.log("Liste");
        // console.log(listeTweet);

    } else {
        await followMirroir.insert({
            'username': name,
            'latestDate': tweetDate[0]
        });

        //The tweetDate and retweet button as the same size and are mirroir of each other
        for (let i = 0; i < retweetButton.length; i++) {
            const date = new Date(tweetDate[i]);
            const button = retweetButton[i];

            // console.log(date);
            // console.log(button);

            if ((new Date()) - date < (3600000 * 24) * maxDayRetweet) {
                listeTweet.push({
                    button,
                    'date': date.toString()
                });
            } else {
                break;
            }
        }
    }
    return listeTweet;


}


module.exports = {
    findDivByText,
    findDivByTextContain,
    findDivByProperties,
    getAllTweetOf,
    getAllTweetOnPage
}